import React from 'react';
import { Route, Switch } from 'react-router-dom';
import ChatBot from './containers/chatBot';
import Login from './containers/login';

export function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={Login} />
      <Route path="/chatBot" exact component={ChatBot} />
    </Switch>
  );
}
