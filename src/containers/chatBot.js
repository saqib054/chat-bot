import { Box, Divider } from '@material-ui/core';
import React, { useEffect, useRef, useState } from 'react';
import { io } from 'socket.io-client';
import ChatMessage from '../components/chatMessage';
import Widgets from '../components/chatWidgets';
import TopNav from '../components/topNav';
import ChatInput from '../components/chatInput';

const ChatBot = () => {
  const [chatMessages, setChatMessages] = useState([]);
  const [userInput, setUserInput] = useState('');
  const [commandMessage, setCommandMessage] = useState();
  const [showWidget, setShowWidget] = useState(true);
  const [checked, setChecked] = useState(false);

  const socketRef = useRef();
  const endOfMessages = useRef(null);

  const scrollToBottom = () => {
    endOfMessages.current.scrollIntoView({ behavior: 'smooth' });
  };
  useEffect(scrollToBottom, [chatMessages, commandMessage]);

  const handleSendCommand = () => {
    socketRef.current.emit('command', {
      author: 'Client',
      message: userInput,
    });
    setShowWidget(true);
  };

  const handleEmitMessage = (message) => {
    socketRef.current.emit('message', {
      author: 'Client',
      message,
    });
  };

  const handleSendMessage = async (e) => {
    if (!userInput) return;
    setChatMessages((chatMessages) => [
      ...chatMessages,
      { author: 'client', message: userInput },
    ]);

    handleEmitMessage(userInput);
    setUserInput('');
    setChecked(true);
    setShowWidget(false);
  };

  useEffect(() => {
    socketRef.current = io(process.env.REACT_APP_CHAT_BOT_URL);

    socketRef.current.on('message', (message) => {
      setChatMessages((chatMessages) => [...chatMessages, message]);
    });

    socketRef.current.on('command', (commandMessage) => {
      setCommandMessage(commandMessage);
    });
  }, []);

  return (
    <>
      <TopNav />
      <Box>
        <Box display={'flex'} flexDirection={'column'}>
          <Box my={2} height={'75vh'} overflow={'auto'}>
            {chatMessages.length > 0 &&
              chatMessages.map((chatMessage, index) => {
                return (
                  <Box
                    mx={1}
                    my={2}
                    key={`${Math.floor(Math.random() * 10)}${index}`}
                  >
                    <ChatMessage chatMessage={chatMessage} checked={checked} />
                  </Box>
                );
              })}

            <Box mx={1} my={1}>
              <Widgets
                showWidget={showWidget}
                setShowWidget={setShowWidget}
                handleEmitMessage={handleEmitMessage}
                commandMessage={commandMessage}
              />
            </Box>

            {/* This is empty div, used to add scroll behaviour  */}
            <Box ref={endOfMessages} />
          </Box>

          <Divider />
          <ChatInput
            userInput={userInput}
            setUserInput={setUserInput}
            handleSendMessage={handleSendMessage}
            handleSendCommand={handleSendCommand}
          />
        </Box>
      </Box>
    </>
  );
};

export default ChatBot;
