import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import { useHistory } from 'react-router-dom';
import { Box, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

const TopNav = () => {
  const classes = useStyles();
  const history = useHistory();

  return (
    <Box className={classes.root} mb={6}>
      <AppBar>
        <Toolbar>
          <Typography variant={'h6'} className={classes.title}>
            ChatBot
          </Typography>
          <Button color="inherit" onClick={() => history.push('/')}>
            Logout
          </Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default TopNav;
