import React from 'react';
import { Box, TextField, InputAdornment, Tooltip } from '@material-ui/core';
import SendIcon from '@material-ui/icons/Send';
import WidgetsIcon from '@material-ui/icons/Widgets';

const ChatInput = ({
  userInput,
  setUserInput,
  handleSendMessage,
  handleSendCommand,
}) => {
  return (
    <>
      <Box mt={4} display={'flex'} alignItems={'center'}>
        <Box width={'100%'} mr={2}>
          <TextField
            placeholder="Type a message"
            variant="outlined"
            fullWidth
            size={'small'}
            value={userInput}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <Box style={{ cursor: 'pointer' }} display={'flex'}>
                    <Tooltip
                      title={
                        userInput
                          ? 'Send message'
                          : 'Type a message and then click send icon'
                      }
                      placement="top"
                      arrow
                    >
                      <Box onClick={handleSendMessage}>
                        <SendIcon />
                      </Box>
                    </Tooltip>
                    <Tooltip title="Send command" placement="top" arrow>
                      <Box ml={2} onClick={handleSendCommand}>
                        <WidgetsIcon />
                      </Box>
                    </Tooltip>
                  </Box>
                </InputAdornment>
              ),
            }}
            onChange={(e) => {
              setUserInput(e.target.value);
            }}
          />
        </Box>
      </Box>
    </>
  );
};

export default ChatInput;
