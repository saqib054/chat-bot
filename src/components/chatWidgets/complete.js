import { Box, Button, Paper, Typography } from '@material-ui/core';
import React from 'react';

const CompleteWidget = ({ complete, handleEmitMessage, setShowWidget }) => {
  const { data } = complete;
  return (
    <>
      <Paper elevation={4}>
        <Box
          display={'flex'}
          flexDirection={'column'}
          px={2}
          py={1}
          width={'100%'}
        >
          <Box mb={1}>
            <Typography>{'Do you want to close the chat?'}</Typography>
          </Box>
          <Box display={'flex'} flexDirection={'row'}>
            {data &&
              data.map((button, index) => (
                <Box ml={1} key={`complete-${index}`}>
                  <Button
                    color={'primary'}
                    variant={button === 'Yes' ? 'contained' : 'outlined'}
                    onClick={(e) => {
                      handleEmitMessage(button);
                      setShowWidget(false);
                    }}
                  >
                    {button}
                  </Button>
                </Box>
              ))}
          </Box>
        </Box>
      </Paper>
    </>
  );
};

export default CompleteWidget;
