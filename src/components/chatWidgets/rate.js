import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';
import { Box, Typography, Paper } from '@material-ui/core';

const ratingLabels = {
  1: 'Useless',
  2: 'Poor',
  3: 'Ok',
  4: 'Good',
  5: 'Excellent',
};

const RateWidget = ({ rating, handleEmitMessage, setShowWidget }) => {
  const [value, setValue] = React.useState(2);
  const [hover, setHover] = React.useState(-1);
  const { data } = rating;

  return (
    <Paper elevation={4}>
      <Box
        display={'flex'}
        alignItems={'flex-start'}
        flexDirection={'column'}
        px={2}
        py={1}
        height={80}
      >
        <Box mb={1}>
          <Typography>{'Please rate your experience'}</Typography>
        </Box>
        <Rating
          name="hover-feedback"
          value={value}
          precision={0.5}
          max={data[1]}
          onChange={(event, newValue) => {
            setValue(newValue);
            handleEmitMessage(newValue);
            setShowWidget(false);
          }}
          onChangeActive={(event, newHover) => {
            setHover(newHover);
          }}
        />
        {value !== null && (
          <Box ml={2}>{ratingLabels[hover !== -1 ? hover : value]}</Box>
        )}
      </Box>
    </Paper>
  );
};

export default RateWidget;
