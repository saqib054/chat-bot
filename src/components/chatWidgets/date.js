import React, { useEffect, useState } from 'react';
import { Box, Typography, Button, Paper } from '@material-ui/core';
import moment from 'moment';

const DateWidget = ({ date, handleEmitMessage, setShowWidget }) => {
  const { data } = date;
  const daysRequired = 7;
  const [days, setDays] = useState([]);

  const getDays = () => {
    for (let i = 0; i <= daysRequired - 1; i += 1) {
      setDays((days) => [...days, moment(data).add(i, 'days').format('dddd')]);
    }
  };

  useEffect(() => {
    getDays();
  }, []);

  return (
    <>
      <Paper elevation={4}>
        <Box
          display={'flex'}
          flexDirection={'column'}
          px={2}
          py={1}
          width={'100%'}
        >
          <Typography>{'Days list'}</Typography>
          <Typography>{'Please select a day'}</Typography>
          <Box display={'flex'} flexDirection={'row'} flexWrap={'wrap'} mt={2}>
            {days.length > 0 &&
              days.map((day, index) => (
                <Box key={`day-${index}`} mt={2} mr={2}>
                  <Button
                    color={'primary'}
                    variant={'outlined'}
                    onClick={(e) => {
                      handleEmitMessage(day);
                      setShowWidget(false);
                    }}
                  >
                    {day}
                  </Button>
                </Box>
              ))}
          </Box>
        </Box>
      </Paper>
    </>
  );
};

export default DateWidget;
