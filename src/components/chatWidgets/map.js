import { makeStyles } from '@material-ui/core/styles';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import React from 'react';
import { MapContainer, Marker, Popup, TileLayer } from 'react-leaflet';
import mapMarker from '../../assets/mapMarker.svg';
import { Box, Paper, Typography } from '@material-ui/core';

const iconPerson = new L.Icon({
  iconUrl: mapMarker,
  iconRetinaUrl: mapMarker,
  iconAnchor: [32, 64],
  popupAnchor: [-0, -0],
  iconSize: [75, 75],
});

const useStyles = makeStyles({
  leafletContainer: {
    maxWidth: 800,
    height: 400,
    margin: '0 auto',
  },
});

const MapWidget = ({ coordinates }) => {
  const classes = useStyles();
  const { lat, lng } = coordinates.data;

  const center = [lat, lng];
  const position = [lat, lng];

  return (
    <>
      <Paper elevation={4}>
        <Box display={'flex'} px={2} py={1}>
          <Box mb={1}>
            <Typography>{'Location sent by bot'}</Typography>
          </Box>
        </Box>
        <MapContainer
          className={classes.leafletContainer}
          center={center}
          zoom={10}
        >
          <TileLayer
            maxZoom="20"
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <Marker position={position} icon={iconPerson}>
            <Popup>
              A pretty CSS3 popup. <br /> Easily customizable.
            </Popup>
          </Marker>
        </MapContainer>
      </Paper>
    </>
  );
};

export default MapWidget;
