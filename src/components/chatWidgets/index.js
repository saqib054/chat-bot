import { Grid } from '@material-ui/core';
import React from 'react';
import CompleteWidget from './complete';
import DateWidget from './date';
import MapWidget from './map';
import RateWidget from './rate';

const Widgets = ({
  showWidget,
  setShowWidget,
  commandMessage = { command: {} },
  handleEmitMessage,
}) => {
  const { command } = commandMessage;
  return (
    <>
      <Grid container direction="column">
        <Grid item zeroMinWidth xs={12} sm={6} md={6}>
          {command?.type === 'date' && showWidget && (
            <DateWidget
              date={command}
              handleEmitMessage={handleEmitMessage}
              setShowWidget={setShowWidget}
            />
          )}

          {command?.type === 'rate' && showWidget && (
            <RateWidget
              rating={command}
              handleEmitMessage={handleEmitMessage}
              setShowWidget={setShowWidget}
            />
          )}

          {command?.type === 'complete' && showWidget && (
            <CompleteWidget
              complete={command}
              handleEmitMessage={handleEmitMessage}
              setShowWidget={setShowWidget}
            />
          )}
          {command?.type === 'map' && showWidget && (
            <MapWidget
              coordinates={command}
              handleEmitMessage={handleEmitMessage}
              setShowWidget={setShowWidget}
            />
          )}
        </Grid>
      </Grid>
    </>
  );
};

export default Widgets;
