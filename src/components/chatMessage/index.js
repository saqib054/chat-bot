import {
  Avatar,
  Grid,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Paper,
  Box,
} from '@material-ui/core';
import { capitalize } from 'lodash';
import React from 'react';

const ChatMessage = ({ chatMessage }) => {
  const { author, message } = chatMessage;
  return (
    <>
      <Grid container direction="row">
        {author === 'client' && <Grid item sm={6} md={6}></Grid>}
        <Grid item zeroMinWidth xs={12} sm={6} md={6}>
          <Paper elevation={4}>
            <ListItem alignItems="flex-start">
              <ListItemAvatar>
                <Avatar />
              </ListItemAvatar>
              <ListItemText
                primary={capitalize(author)}
                secondary={
                  <>{chatMessage?.message && <span>{message}</span>}</>
                }
              />
            </ListItem>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};

export default ChatMessage;
